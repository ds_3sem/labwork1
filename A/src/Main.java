import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main {
    private static Thread Tthread1;
    private static Thread Tthread2;

    private static boolean increasePriority(Thread thread) {
        int currentPriority = thread.getPriority();
        if (currentPriority < Thread.MAX_PRIORITY) {
            thread.setPriority(currentPriority + 1);
            return true;
        }
        return false;
    }

    private static boolean decreasePriority(Thread thread) {
        int currentPriority = thread.getPriority();
        if (currentPriority > Thread.MIN_PRIORITY) {
            thread.setPriority(currentPriority - 1);
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        JFrame appFrame = new JFrame("Task A");
        appFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        appFrame.setSize(300, 210);

        Font font1 = new Font(Font.SERIF, Font.BOLD, 20);
        Font font2 = new Font(Font.SERIF, Font.PLAIN, 15);
        Font font3 = new Font(Font.SERIF, Font.PLAIN, 10);

        JButton startButton = new JButton("Start");
        startButton.setFont(font1);
        JButton incPrior1 = new JButton(">");
        incPrior1.setFont(font3);
        JButton decPrior1 = new JButton("<");
        decPrior1.setFont(font3);
        JButton incPrior2 = new JButton(">");
        incPrior2.setFont(font3);
        JButton decPrior2 = new JButton("<");
        decPrior2.setFont(font3);

        JSlider slider = new JSlider();
        slider.setMajorTickSpacing(10);
        slider.setMinorTickSpacing(5);
        slider.setPaintLabels(true);
        slider.setPaintTicks(true);
        slider.setFont(font3);

        JLabel label1 = new JLabel("Thread 1 priority:");
        label1.setFont(font2);
        JTextField priority1 = new JTextField("1", 7);
        JLabel label2 = new JLabel("Thread 2 priority:");
        label2.setFont(font2);
        JTextField priority2 = new JTextField("10", 7);

        final int[] threadPriority1 = {Integer.parseInt(priority1.getText())};
        int targetPosition1 = 10;
        final int[] threadPriority2 = {Integer.parseInt(priority2.getText())};
        int targetPosition2 = 90;

        incPrior1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (increasePriority(Tthread1)) {
                    int pr = Integer.parseInt(priority1.getText());
                    pr++;
                    priority1.setText(String.valueOf(pr));
                    threadPriority1[0] = pr;
                }
            }
        });

        decPrior1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (decreasePriority(Tthread1)) {
                    int pr = Integer.parseInt(priority1.getText());
                    pr--;
                    priority1.setText(String.valueOf(pr));
                    threadPriority1[0] = pr;
                }
            }
        });

        incPrior2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (increasePriority(Tthread2)) {
                    int pr = Integer.parseInt(priority2.getText());
                    pr++;
                    priority2.setText(String.valueOf(pr));
                    threadPriority2[0] = pr;
                }
            }
        });

        decPrior2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (decreasePriority(Tthread2)) {
                    int pr = Integer.parseInt(priority2.getText());
                    pr--;
                    priority2.setText(String.valueOf(pr));
                    threadPriority2[0] = pr;
                }
            }
        });

        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Tthread1 = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (!Thread.currentThread().isInterrupted()) {
                            try {
                                int position = slider.getValue();
                                if (position > targetPosition1) {
                                    position -= 1;
                                    slider.setValue(position);
                                }
                                Thread.sleep(1000 / threadPriority1[0]);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                                break;
                            }
                        }
                    }
                });

                Tthread2 = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (!Thread.currentThread().isInterrupted()) {
                            try {
                                int position = slider.getValue();
                                if (position < targetPosition2) {
                                    position += 1;
                                    slider.setValue(position);
                                }
                                Thread.sleep(1000 / threadPriority2[0]);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                                break;
                            }
                        }
                    }
                });

                Tthread1.setPriority(threadPriority1[0]);
                Tthread2.setPriority(threadPriority2[0]);

                Tthread1.start();
                Tthread2.start();

                startButton.setEnabled(false);
            }
        });

        JPanel panel = new JPanel();
        panel.add(slider);
        panel.add(label1);
        panel.add(priority1);
        panel.add(decPrior1);
        panel.add(incPrior1);
        panel.add(label2);
        panel.add(priority2);
        panel.add(decPrior2);
        panel.add(incPrior2);
        panel.add(startButton);

        appFrame.add(panel);
        appFrame.setVisible(true);
    }
}