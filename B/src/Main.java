import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main {
    private static Thread Tthread1;
    private static Thread Tthread2;
    private static int semaphore = 0;

    public static void main(String[] args) {
        JFrame appFrame = new JFrame("Task B");
        appFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        appFrame.setSize(240, 210);

        Font font1 = new Font(Font.SERIF, Font.PLAIN, 15);
        Font font2 = new Font(Font.SERIF, Font.PLAIN, 10);

        JButton startButton1 = new JButton("Start 1");
        JButton startButton2 = new JButton("Start 2");
        JButton stopButton1 = new JButton("Stop 1");
        JButton stopButton2 = new JButton("Stop 2");
        stopButton1.setEnabled(false);
        stopButton2.setEnabled(false);

        startButton1.setFont(font1);
        startButton2.setFont(font1);
        stopButton1.setFont(font1);
        stopButton2.setFont(font1);

        JSlider slider = new JSlider();
        slider.setMajorTickSpacing(10);
        slider.setMinorTickSpacing(5);
        slider.setPaintLabels(true);
        slider.setPaintTicks(true);
        slider.setFont(font2);

        startButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (semaphore == 0) {
                    semaphore = 1;
                    Tthread1 = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            System.out.println("Thread 1 started with priority 1");
                            Tthread1.setPriority(Thread.MIN_PRIORITY);
                            while (semaphore == 1) {
                                try {
                                    int position = slider.getValue();
                                    if (position > 10) {
                                        position -= 1;
                                        slider.setValue(position);
                                    }
                                    Thread.sleep(500);
                                } catch (InterruptedException ex) {
                                    break;
                                }
                            }
                            System.out.println("Thread 1 stopped");
                        }
                    });
                    Tthread1.start();
                    stopButton1.setEnabled(true);
                    startButton1.setEnabled(false);
                    stopButton2.setEnabled(false);
                    startButton2.setEnabled(true);
                } else {
                    JOptionPane.showMessageDialog(null, "Thread 2 is running");
                }
            }
        });

        startButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (semaphore == 0) {
                    semaphore = 2;
                    Tthread2 = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            System.out.println("Thread 2 started with priority 10");
                            Tthread2.setPriority(Thread.MAX_PRIORITY);
                            while (semaphore == 2) {
                                try {
                                    int position = slider.getValue();
                                    if (position < 90) {
                                        position += 1;
                                        slider.setValue(position);
                                    }
                                    Thread.sleep(500);
                                } catch (InterruptedException ex) {
                                    break;
                                }
                            }
                            System.out.println("Thread 2 stopped");
                        }
                    });
                    Tthread2.start();
                    stopButton2.setEnabled(true);
                    startButton2.setEnabled(false);
                    stopButton1.setEnabled(false);
                    startButton1.setEnabled(true);
                } else {
                    JOptionPane.showMessageDialog(null, "Thread 1 is running!");
                }
            }
        });

        stopButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Tthread1.interrupt();
                Tthread1 = null;
                semaphore = 0;
                startButton1.setEnabled(true);
                stopButton1.setEnabled(false);
                stopButton2.setEnabled(false);
            }
        });

        stopButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Tthread2.interrupt();
                Tthread2 = null;
                semaphore = 0;
                startButton2.setEnabled(true);
                stopButton2.setEnabled(false);
                stopButton1.setEnabled(false);
            }
        });

        JPanel panel = new JPanel();
        panel.add(slider);
        panel.add(startButton1);
        panel.add(startButton2);
        panel.add(stopButton1);
        panel.add(stopButton2);

        appFrame.add(panel);
        appFrame.setVisible(true);
    }
}